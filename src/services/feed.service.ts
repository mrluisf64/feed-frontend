import axios, { AxiosResponse } from "axios";

class FeedService {
  hits(): Promise<AxiosResponse> {
    return axios.get(process.env.REACT_APP_URL + "hits");
  }

  destroyHit(id: string): Promise<AxiosResponse> {
    return axios.delete(process.env.REACT_APP_URL + "hits/" + id);
  }
}

export default new FeedService();
